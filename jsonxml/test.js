﻿var assert = require('assert');
var jsonxml = require('.');
var Element = jsonxml.Element;
var Property = jsonxml.Property;
var Attribute = jsonxml.Attribute;
var Text = jsonxml.Text;
exports['default'] = function () {
	var type = new Element('name', {
		attribute: new Attribute(new Text()),
		property: new Property(new Text())
	});
	var data = {
		attribute: 'attribute',
		property: 'property'
	};
	var xml = jsonxml(data, type);
	assert.deepEqual(xml, {
		name: 'name',
		attributes: { attribute: 'attribute' },
		inner: [{ name: 'property', inner: 'property' }]
	});
}