﻿function jsonxml(data, type) {
	switch (type.$type) {
		case 'text':
			return data.toString();
		case 'element':
			var name = type.name;
			var attributes = {};
			var children = [];
			var text;
			for (var i in data) {
				var property = type.properties[i];
				var value = jsonxml(data[i], property.type);
				switch (property.$type) {
					case 'text':
						text = value;
						break;
					case 'attribute':
						attributes[i] = value;
						break;
					case 'property':
						children.push(
							{
								name: i,
								inner: typeof value == 'string'?value:[value]
							}
						);
						break;
				}
			}
			return {
				name: name,
				attributes: attributes,
				inner : text != undefined?text:children
			};
	}
}
function Element(name, properties) {
	this.name = name;
	this.properties = properties;
}
Element.prototype.$type = 'element';
function Attribute(type) {
	this.type = type;
}
Attribute.prototype.$type = 'attribute';
function Property(type) {
	this.type = type;
}
Property.prototype.$type = 'property';
function Text() { }
Text.prototype.$type = 'text';
jsonxml.Element = Element;
jsonxml.Attribute = Attribute;
jsonxml.Property = Property;
jsonxml.Text = Text;
module.exports = jsonxml;